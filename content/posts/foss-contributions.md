---
title: "Contributions to FOSS"
# date: 2020-09-15T11:30:03+00:00
# weight: 1
# aliases: ["/first"]
# tags: ["first"]
author: "greg"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: false
description: "Description"
canonicalURL: "https://greg.gitlab.io/blog/post"
# disableHLJS: true # to disable highlightjs
disableShare: true
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: true
# cover:
#     image: "<image path/url>" # image path/url
#     alt: "<alt text>" # alt text
#     caption: "<text>" # display caption under cover
#     relative: false # when using page bundles set this to true
#     hidden: true # only hide on current single page
# editPost:
#     URL: "https://gitlab.com/greg/blog/content"
#     Text: "Suggest Changes" # edit text
#     appendFilePath: true # to append file path to Edit link
---

I have contributed to the following open source projects:

- [OpenSCAP/openscap](https://github.com/OpenSCAP/openscap): Open-source tool for implementing Security Content Automation Protocol (SCAP) for system security management.
- [crowdsecurity/crowdsec](https://github.com/crowdsecurity/crowdsec): An open-source, lightweight software enabling users to detect and respond to bad behaviours, leveraging crowd-sourced IP reputation.
- [google/oss-fuzz](https://github.com/google/oss-fuzz): Google's initiative for continuous fuzzing of open-source software, enhancing system security and reliability.
- [google/osv-scanner](https://github.com/google/osv-scanner): A tool for scanning open-source software dependencies for known vulnerabilities.
- [mattermost/mattermost-server](https://github.com/mattermost/mattermost-server): Open-source, self-hosted online chat service with file sharing, search, and integrations.
- [nodesecurity/eslint-plugin-security](https://github.com/nodesecurity/eslint-plugin-security): ESLint plugin for Node.js security, helping developers produce secure code.
- [gorhill/uBlock Origin](https://github.com/gorhill/uBlock): An efficient blocker for Chromium and Firefox, blocking ads, trackers, and malware sites.
- [snort3/snort3](https://github.com/snort3/snort3): Next generation Snort Intrusion Prevention System (IPS) with new features and improved performance.
- [offensive-security/exploitdb](https://github.com/offensive-security/exploitdb): Repository of the Exploit Database, a collection of exploits, shellcodes, and security papers.
- [robertdavidgraham/masscan](https://github.com/robertdavidgraham/masscan): An ultra-fast TCP port scanner, capable of scanning the entire Internet in under 6 minutes.
- [evilmartians/lefthook](https://github.com/evilmartians/lefthook): A robust task runner for Git hooks, ensuring code quality by running linters, tests, and other checks.
- [accurics/terrascan](https://github.com/accurics/terrascan): A comprehensive policy as code (PaC) compliance and security violation detection tool for Infrastructure as Code (IaC).
- [trivyaquasecurity/trivy](https://github.com/aquasecurity/trivy): A simple and comprehensive vulnerability scanner for containers and other artifacts, suitable for CI.
- [returntocorp/semgrep](https://github.com/returntocorp/semgrep): A customizable, lightweight static analysis tool for many languages, finding bugs using patterns that code authors already know.
- [aquasecurity/tfsec](https://github.com/aquasecurity/tfsec): A security scanner for Terraform code, identifying potential security issues.
- [greenbone/openvas-scanner](https://github.com/greenbone/openvas-scanner): Part of Greenbone Vulnerability Management (GVM), this is a full-featured scan engine that executes a continuously updated and extended feed of Network Vulnerability Tests (NVTs).
- [zricethezav/gitleaks](https://github.com/zricethezav/gitleaks): A SAST tool for detecting hardcoded secrets like passwords, API keys, and tokens in git repos.
- [CISOfy/lynis](https://github.com/CISOfy/lynis): A security auditing tool for Linux, macOS, and UNIX-based systems, assisting with compliance testing and system hardening.
- [VSCodium/vscodium](https://github.com/VSCodium/vscodium): Binary releases of VS Code without Microsoft's branding, telemetry, and licensing, providing a community-driven default configuration.
- [grafana/grafana](https://github.com/grafana/grafana/): An open-source platform for monitoring and observability, allowing users to query, visualize, alert on, and understand metrics from various sources.
- [nicolargo/glances](https://github.com/nicolargo/glances): A cross-platform system monitoring tool written in Python.
- [osquery/osquery](https://github.com/osquery/osquery): An operating system instrumentation framework for Windows, OS X (macOS), Linux, and FreeBSD.
- [Homebrew/brew](https://github.com/Homebrew/brew): The package manager for macOS (or Linux), providing software installation functionality.
- [go-gitea/gitea](https://github.com/go-gitea/gitea): A lightweight, self-hosted Git service.
- [david-a-wheeler/flawfinder](https://github.com/david-a-wheeler/flawfinder): A static analysis tool for finding vulnerabilities in C/C++ source code.
- [httpie/httpie](https://github.com/httpie/httpie): A user-friendly command-line HTTP client for the API era.
- [asdf-vm/asdf](https://github.com/asdf-vm/asdf): A CLI tool managing multiple language runtime versions on a per-project basis.
- [servo/servo](https://github.com/servo/servo): A high-performance browser engine designed for application and embedded use.
- [ChainSafe/web3.js](https://github.com/ChainSafe/web3.js): Ethereum JavaScript API facilitating interaction with the Ethereum blockchain.
- [finos/git-proxy](https://github.com/finos/git-proxy): A compliance-friendly proxy for highly regulated industries to accept open source.
- [ethereum/go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol.
- [kalilinux/documentation/kali-docs](https://gitlab.com/kalilinux/documentation/kali-docs): The official documentation for Kali Linux, a popular distribution for cybersecurity professionals.
- [ubuntu-mate/ubuntu-mate.org](https://github.com/ubuntu-mate/ubuntu-mate.org): The repository for the Ubuntu MATE operating system's website.